﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ScenarioEditor.Model;
using ScenarioEditor.Properties;

namespace ScenarioEditor
{
    public class ElementManager
    {
        private readonly Canvas _mainCanvas;
        private readonly ContextMenu _vertexContextMenu;

        private readonly List<EllipseItem> _vertexes = new List<EllipseItem>();
        private readonly List<EdgeItem> _edges = new List<EdgeItem>();
        private readonly List<RectItem> _actions = new List<RectItem>();

        public ElementManager()
        {
            _mainCanvas = MainWindowResources.GetCanvas(Resources.MainCanvas);
            _vertexContextMenu = MainWindowResources.GetContextMenu(Resources.ItemContextMenu);
        }

        public List<EllipseItem> Vertexes => _vertexes;
        public List<EdgeItem> Edges => _edges;
        public List<RectItem> Actions => _actions;
        public EdgeItem CurrentEdge { get; set; }

        public void AddNewEllipseVertex()
        {
            var mySolidColorBrush = new SolidColorBrush { Color = Common.DarkGrayColor };

            var vertex = new Ellipse
            {
                Height = Common.VertexDiameter,
                Width = Common.VertexDiameter,
                Fill = mySolidColorBrush,
                StrokeThickness = Common.StrokeThickness,
                Stroke = Common.BlackBrush,
                ContextMenu = _vertexContextMenu
            };

            var x = MousePosition.CurrentMousePosition.X - Common.VertexRadius;
            var y = MousePosition.CurrentMousePosition.Y - Common.VertexRadius;

            UpdateWindow(MousePosition.CurrentMousePosition);

            AddElementToCanvas(vertex, x, y);
        }

        public void AddNewRectVertex(Point mousePos, IShapeItem vertexUnderMouse, IShapeItem initialVertex, Line edge)
        {
            var mySolidColorBrush = new SolidColorBrush { Color = Common.DarkGrayColor };

            var vertex = new Rectangle
            {
                Height = Common.VertexDiameter,
                Width = Common.VertexDiameter,
                Fill = mySolidColorBrush,
                StrokeThickness = Common.StrokeThickness,
                Stroke = Common.BlackBrush,
                ContextMenu = _vertexContextMenu
            };

            var x = mousePos.X - Common.VertexRadius;
            var y = mousePos.Y - Common.VertexRadius;

            UpdateWindow(mousePos);

            AddElementToCanvas(vertex, x, y, vertexUnderMouse.Episode, initialVertex.Episode, edge);
        }

        public bool HasItems()
        {
            return _vertexes.Count > 0;
        }

        public void RemoveVertex(IShapeItem vertexUnderMouse)
        {
            if (vertexUnderMouse is EllipseItem ellipseItem)
            {
                RemoveEllipse(ellipseItem);
            }
            else if (vertexUnderMouse is RectItem rectItem)
            {
                RemoveRect(rectItem);
            }
        }

        private void AddElementToCanvas(Shape element, double x, double y, Episode episode = null,
            Episode initialVertexEpisode = null, Line edge = null)
        {
            Canvas.SetTop(element, y);
            Canvas.SetLeft(element, x);

            _mainCanvas.Children.Add(element);

            if (element is Ellipse ellipse)
                _vertexes.Add(new EllipseItem { Vertex = ellipse });
            else if (element is Rectangle rectangle)
            {
                var newVertex = new RectItem {
                    Vertex = rectangle,
                    Edge = CurrentEdge};
                newVertex.Episode.Actions.Add(episode);
                newVertex.Episode.PreviousEpisode = initialVertexEpisode;
                _actions.Add(newVertex);
                initialVertexEpisode?.Actions.Add(newVertex.Episode);

                episode.PreviousEpisode = newVertex.Episode;
            }
        }

        private void UpdateWindow(Point mousePos)
        {
            if (mousePos.X >= _mainCanvas.ActualWidth - Common.MarginValue)
            {
                _mainCanvas.Width = _mainCanvas.ActualWidth + Common.ResizeValue;
            }

            if (mousePos.Y >= _mainCanvas.ActualHeight - Common.MarginValue)
            {
                _mainCanvas.Height = _mainCanvas.ActualHeight + Common.ResizeValue;
            }
        }

        private void RemoveRect(RectItem rectItem)
        {
            _actions.Remove(rectItem);
            _edges.Remove(rectItem.Edge);
            
            _mainCanvas.Children.Remove(rectItem.Vertex);
            _mainCanvas.Children.Remove(rectItem.Edge.Edge);
            
            rectItem.Episode.PreviousEpisode.Actions.Remove(rectItem.Episode);
            rectItem.Episode.Actions[0].PreviousEpisode = null;
        }

        private void RemoveEllipse(EllipseItem ellipseItem)
        {
            foreach (var action in ellipseItem.Episode.Actions)
            {
                FindAndRemoveRect(action);
            }

            //todo: replace foreach
            var item = ellipseItem.Episode.PreviousEpisode;
            FindAndRemoveRect(item);

            _vertexes.Remove(ellipseItem);
            
            _mainCanvas.Children.Remove(ellipseItem.Vertex);

        }

        private void FindAndRemoveRect(Episode action)
        {
            var rectItem = FindRectItem(action);

            if (rectItem != null)
                RemoveRect(rectItem);
        }

        private RectItem FindRectItem(Episode episode)
        {
            foreach (var item in _actions)
            {
                if (item.Episode.Equals(episode))
                    return item;
            }

            return null;
        }
    }
}
