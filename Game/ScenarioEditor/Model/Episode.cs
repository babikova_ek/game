﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ScenarioEditor.Model
{
    [DataContract]
    public class Episode
    {
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public IList<Episode> Actions { get; set; } = new List<Episode>();

        public Episode PreviousEpisode { get; set; }
    }
}
