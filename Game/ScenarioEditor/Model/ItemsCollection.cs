﻿using System.Windows.Shapes;

namespace ScenarioEditor.Model
{
    public interface IShapeItem
    {
        Shape Vertex { get; set; }
        Episode Episode { get; set; }
    }

    public class EllipseItem : IShapeItem
    {
        public Shape Vertex { get; set; }
        public Episode Episode { get; set; } = new Episode { Description = string.Empty };
    }

    public class RectItem : IShapeItem
    {
        public Shape Vertex { get; set; }
        public Episode Episode { get; set; } = new Episode { Description = string.Empty };
        public EdgeItem Edge { get; set; }
    }

    public class EdgeItem
    {
        public Line Edge { get; set; }
    }
}
