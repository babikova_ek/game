﻿using System.Windows.Input;
using Prism.Commands;
using ScenarioEditor.Dialogs;

namespace ScenarioEditor.Commands
{
    public class ContextMenuCommands
    {
        private readonly ElementManager _elementManager;
        private readonly CustomDialogs _dialogs;

        public ContextMenuCommands(ElementManager elementManager, CustomDialogs dialogs)
        {
            _elementManager = elementManager;
            _dialogs = dialogs;

            EditCommand = new DelegateCommand(EditVertex);
            RemoveCommand = new DelegateCommand(RemoveVertex);
        }

        public ICommand EditCommand { get; }
        public ICommand RemoveCommand { get; }

        private void EditVertex()
        {
            var hasVertexUnderMouse = Common.TryGetAnyVertexUnderMouse(MousePosition.RightButtonClickPosition, _elementManager, out var vertexUnderMouse);

            if (hasVertexUnderMouse)
                Common.EditTextInVertex(vertexUnderMouse, _dialogs);
        }

        private void RemoveVertex()
        {
            var hasVertexUnderMouse = Common.TryGetAnyVertexUnderMouse(MousePosition.RightButtonClickPosition, _elementManager, out var vertexUnderMouse);

            if (hasVertexUnderMouse)
                _elementManager.RemoveVertex(vertexUnderMouse);
        }
    }
}
