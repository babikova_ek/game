﻿using System;
using System.IO;
using System.Windows.Input;
using Prism.Commands;
using ScenarioEditor.Dialogs;
using ScenarioEditor.Properties;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace ScenarioEditor.Commands
{
    public class MenuCommands
    {
        private readonly DelegateCommand _openCommand;
        private readonly DelegateCommand _saveCommand;
        private readonly DelegateCommand _saveAsCommand;
        private readonly DelegateCommand _closeCommand;
        private readonly DelegateCommand _exitCommand;

        private string _currentFileName;
        private FileStream _currentStream;
        private readonly ElementManager _elementManager;
        private readonly CustomDialogs _dialogs;

        public MenuCommands(ElementManager elementManager, CustomDialogs dialogs, Action close)
        {
            _elementManager = elementManager;
            _dialogs = dialogs;

            _openCommand = new DelegateCommand(Open);
            _saveCommand = new DelegateCommand(Save, CanSave);
            _saveAsCommand = new DelegateCommand(SaveAs, CanSave);
            _closeCommand = new DelegateCommand(Close, CanClose);
            _exitCommand = new DelegateCommand(close);
        }

        public ICommand OpenCommand => _openCommand;

        public ICommand SaveCommand => _saveCommand;

        public ICommand SaveAsCommand => _saveAsCommand;

        public ICommand CloseCommand => _closeCommand;

        public ICommand ExitCommand => _exitCommand;

        public void RaiseCanExecute()
        {
            _saveCommand.RaiseCanExecuteChanged();
            _saveAsCommand.RaiseCanExecuteChanged();
            _closeCommand.RaiseCanExecuteChanged();
        }

        private void Open()
        {
            if (!string.IsNullOrEmpty(_currentFileName))
            {
                Close();
            }

            var openDialog = new OpenFileDialog
            {
                DefaultExt = Resources.DefaultExt,
                Filter = Resources.DocumentFilter
            };

            if (openDialog.ShowDialog() == true)
            {
                _currentFileName = openDialog.FileName;
                _currentStream = File.Open(_currentFileName, FileMode.Open, FileAccess.ReadWrite);

                //todo load data from current stream
            }
        }

        private void Save()
        {
            //todo save data to current stream
        }

        private void SaveAs()
        {
            var saveDialog = new SaveFileDialog
            {
                FileName = Resources.DefaultFileName,
                DefaultExt = Resources.DefaultExt,
                Filter = Resources.DocumentFilter
            };

            if (saveDialog.ShowDialog() == true)
            {
                var newFileName = saveDialog.FileName;

                if (_currentFileName.Equals(newFileName))
                {
                    //todo save data to current stream
                }
                else
                {
                    _currentFileName = newFileName;
                    _currentStream.Close();
                    _currentStream = File.Open(_currentFileName, FileMode.Open, FileAccess.ReadWrite);

                    //todo save data to current stream
                }
            }
        }

        private bool CanSave()
        {
            return !string.IsNullOrEmpty(_currentFileName) || _elementManager.HasItems();
        }

        private void Close()
        {
            var message = Resources.SaveCurrentChangesQuestion;
            var caption = Resources.SaveTheChanges;
            
            var result = _dialogs.ShowQuestion(message, caption);

            if (result)
            {
                Save();
                CloseFile();
            }
            else
            {
                CloseFile();
            }
        }

        private void CloseFile()
        {
            _currentFileName = string.Empty;
            _currentStream.Close();

            //todo clean canvas & data
        }

        private bool CanClose()
        {
            return !string.IsNullOrEmpty(_currentFileName);
        }
    }
}