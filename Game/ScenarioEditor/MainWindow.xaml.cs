﻿using System.Windows;
using System.Windows.Input;
using ScenarioEditor.Dialogs;

namespace ScenarioEditor
{
    public partial class MainWindow : Window
    {
        private readonly WindowEventsHandler _windowEventsHandler;
        private readonly MainWindowModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            
            var dialogs = new CustomDialogs(this);
            var resources = new MainWindowResources(this);
            var elementManager = new ElementManager();
            _windowEventsHandler = new WindowEventsHandler(dialogs, elementManager);

            DataContext = _viewModel = new MainWindowModel(elementManager, dialogs, Close);
        }
        
        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _windowEventsHandler.OnMouseLeftButtonDown(e);
            _viewModel.MenuCommands.RaiseCanExecute();
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            _windowEventsHandler.OnMouseMove(e);
        }

        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _windowEventsHandler.OnMouseLeftButtonUp();
        }

        private void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            _windowEventsHandler.OnMouseRightButtonDown(e);
        }
    }
}
