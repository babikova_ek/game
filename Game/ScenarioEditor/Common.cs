﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ScenarioEditor.Dialogs;
using ScenarioEditor.Model;
using ScenarioEditor.Properties;

namespace ScenarioEditor
{
    internal static class Common
    {
        public const int VertexDiameter = 50;
        public const int VertexRadius = VertexDiameter / 2;
        public const int StrokeThickness = 1;
        public const int ResizeValue = 150;
        public const int MarginValue = 50;

        public static Color DarkGrayColor => Colors.DarkGray;
        public static Brush BlackBrush => Brushes.Black;

        public static Point GetVertexCenter(UIElement vertex)
        {
            var vertexY = Canvas.GetTop(vertex) + VertexRadius;
            var vertexX = Canvas.GetLeft(vertex) + VertexRadius;
            return new Point(vertexX, vertexY);
        }

        private static double CalcDistanceToVertexCenter(Point mousePos, UIElement vertex)
        {
            var vertexCenter = GetVertexCenter(vertex);

            return (vertexCenter - mousePos).LengthSquared;
        }

        public static void EditTextInVertex(IShapeItem vertexUnderMouse, CustomDialogs dialogs)
        {
            var text = dialogs.ShowInputDialog(vertexUnderMouse);

            if (text != string.Empty)
            {
                vertexUnderMouse.Episode.Description = text;

                var toolTip = new ToolTip
                {
                    Content = text,
                    Style = MainWindowResources.GetStyle(Resources.MultilineToolTipStyle)
                };

                vertexUnderMouse.Vertex.ToolTip = toolTip;
            }
        }

        public static bool TryGetAnyVertexUnderMouse(Point mousePos, ElementManager elementManager, out IShapeItem vertexUnderMouse)
        {
            vertexUnderMouse = null;

            if (TryGetVertexUnderMouse(MousePosition.CurrentMousePosition, elementManager, out vertexUnderMouse))
                return true;

            foreach (var item in elementManager.Actions)
            {
                if (IsVertexUnderMouse(item.Vertex, mousePos))
                {
                    vertexUnderMouse = item;
                    return true;
                }
            }

            return false;
        }
        
        public static bool TryGetVertexUnderMouse(Point mousePos, ElementManager elementManager, out IShapeItem vertexUnderMouse)
        {
            vertexUnderMouse = null;

            foreach (var item in elementManager.Vertexes)
            {
                if (IsVertexUnderMouse(item.Vertex, mousePos))
                {
                    vertexUnderMouse = item;
                    return true;
                }
            }

            return false;
        }

        private static bool IsVertexUnderMouse(UIElement vertex, Point mousePos)
        {
            var distanceToVertexCenter = CalcDistanceToVertexCenter(mousePos, vertex);

            return distanceToVertexCenter <= VertexDiameter * VertexDiameter;
        }
    }
}
