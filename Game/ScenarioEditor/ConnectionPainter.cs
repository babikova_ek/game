﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using ScenarioEditor.Model;

namespace ScenarioEditor
{
    internal class ConnectionPainter
    {
        private readonly ElementManager _elementManager;
        private readonly Canvas _mainCanvas;
        private Line _currentEdge;
        private IShapeItem _initialVertex;

        public ConnectionPainter(ElementManager elementManager, Canvas mainCanvas)
        {
            _elementManager = elementManager;
            _mainCanvas = mainCanvas;
        }

        public Line StartConnectingVertexes(UIElement vertexUnderMouse)
        {
            var vertexCenter = Common.GetVertexCenter(vertexUnderMouse);

            return new Line
            {
                Stroke = Common.BlackBrush,
                X1 = vertexCenter.X,
                X2 = MousePosition.CurrentMousePosition.X,
                Y1 = vertexCenter.Y,
                Y2 = MousePosition.CurrentMousePosition.Y,
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                StrokeThickness = Common.StrokeThickness
            };
        }

        public void AddNewConnection(IShapeItem vertexUnderMouse, Line currentEdge, IShapeItem initialVertex)
        {
            _currentEdge = currentEdge;
            _initialVertex = initialVertex;

            if (AlreadyHasCurrentEdge() || vertexUnderMouse is RectItem)
            {
                _mainCanvas.Children.RemoveAt(0);
                _elementManager.Edges.RemoveAt(_elementManager.Edges.Count - 1);
            }
            else
            {
                var lengthLine = CalculateLengthLine();

                if (lengthLine > Common.VertexRadius * Common.VertexRadius)
                {
                    var x = (_currentEdge.X2 + _currentEdge.X1) / 2;
                    var y = (_currentEdge.Y2 + _currentEdge.Y1) / 2;
                    _elementManager.AddNewRectVertex(new Point(x, y), vertexUnderMouse, _initialVertex, _currentEdge);
                }
            }
        }

        private double CalculateLengthLine()
        {
            var xLine = _currentEdge.X2 - _currentEdge.X1;
            var yLine = _currentEdge.Y2 - _currentEdge.Y1;

            return xLine * xLine + yLine * yLine;
        }

        private bool AlreadyHasCurrentEdge()
        {
            // todo: refactor this hell
            foreach (var item in _elementManager.Edges)
            {
                if ((item.Edge.X1.Equals(_currentEdge.X1) && item.Edge.X2.Equals(_currentEdge.X2) &&
                     item.Edge.Y1.Equals(_currentEdge.Y1) && item.Edge.Y2.Equals(_currentEdge.Y2) ||
                     item.Edge.X1.Equals(_currentEdge.X2) && item.Edge.X2.Equals(_currentEdge.X1) &&
                     item.Edge.Y1.Equals(_currentEdge.Y2) && item.Edge.Y2.Equals(_currentEdge.Y1)) &&
                    item.Edge != _currentEdge)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
