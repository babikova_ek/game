﻿using System;
using System.Windows.Forms;
using System.Windows.Input;
using Prism.Commands;

namespace ScenarioEditor.Dialogs
{
    public class MessageBoxViewModel
    {
        private readonly Action _close;

        public MessageBoxViewModel(string message, Action close)
        {
            Message = message;
            _close = close;

            YesCommand = new DelegateCommand(Yes);
            NoCommand = new DelegateCommand(No);
        }

        public string Message { get; }

        //todo review
        public DialogResult Result { get; private set; }

        public ICommand YesCommand { get; }

        public ICommand NoCommand { get; }

        private void Yes()
        {
            Result = DialogResult.Yes;
            _close();
        }

        private void No()
        {
            Result = DialogResult.No;
            _close();
        }
    }
}
