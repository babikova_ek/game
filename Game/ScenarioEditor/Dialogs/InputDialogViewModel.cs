﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Prism.Commands;
using ScenarioEditor.Annotations;

namespace ScenarioEditor.Dialogs
{
    internal sealed class InputDialogViewModel : INotifyPropertyChanged
    {
        private string _message;
        private readonly Action _close;
        private string _text;
        private readonly string _oldText;
        private readonly DelegateCommand _okCommand;
        private readonly DelegateCommand _cancelCommand;

        public InputDialogViewModel(string message, string text, Action close)
        {
            _okCommand = new DelegateCommand(Ok, CanOk);
            _cancelCommand = new DelegateCommand(Cancel);

            Message = message;
            Text = text;
            _oldText = text;
            _close = close;
        }

        private void Cancel()
        {
            Text = _oldText;
            _close.Invoke();
        }

        public string Message
        {
            get { return _message; }
            set
            {
                if (_message == value)
                    return;

                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        public string Text
        {
            get { return _text;}
            set
            {
                if (_text == value)
                    return;

                _text = value;
                _okCommand.RaiseCanExecuteChanged();
            }
        }

        public ICommand OkCommand => _okCommand;

        public ICommand CancelCommand => _cancelCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool CanOk()
        {
            return _text?.Length > 0;
        }

        private void Ok()
        {
            _close.Invoke();
        }
    }
}
