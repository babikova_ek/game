﻿using System.Windows;
using ScenarioEditor.Dialogs;

namespace ScenarioEditor
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog(string message, string title, string text, Window mainWindow)
        {
            InitializeComponent();
            Owner = mainWindow;
            Title = title;
            DataContext = new InputDialogViewModel(message, text, Close);
        }
    }
}
