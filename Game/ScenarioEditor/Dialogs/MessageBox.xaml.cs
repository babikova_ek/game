﻿using System.Windows;
using System.Windows.Forms;

namespace ScenarioEditor.Dialogs
{
    /// <summary>
    /// Interaction logic for MessageBox.xaml
    /// </summary>
    public partial class MessageBox : Window
    {
        private readonly MessageBoxViewModel _viewModel;

        public MessageBox(string message, string title, Window mainWindow)
        {
            InitializeComponent();

            Owner = mainWindow;
            Title = title;
            DataContext = _viewModel = new MessageBoxViewModel(message, Close);
        }

        public DialogResult ShowMessage()
        {
            ShowDialog();

            return _viewModel.Result;
        }
    }
}
