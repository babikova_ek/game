﻿using System.Windows;
using System.Windows.Forms;
using ScenarioEditor.Model;
using ScenarioEditor.Properties;

namespace ScenarioEditor.Dialogs
{
    public class CustomDialogs
    {
        private readonly Window _window;

        public CustomDialogs(Window window)
        {
            _window = window;
        }

        public void ShowMessage()
        {

        }

        public bool ShowQuestion(string message, string caption)
        {
            var dialog = new MessageBox(message, caption, _window);
            var result = dialog.ShowMessage();

            return result == DialogResult.Yes;
        }

        public void ShowError()
        {

        }

        public string ShowInputDialog(IShapeItem vertexUnderMouse)
        {
            var inputDialog = new InputDialog(Resources.EditVertexDescription, Resources.EditVertex, vertexUnderMouse.Episode.Description, _window);

            inputDialog.ShowDialog();

            if (!(inputDialog.DataContext is InputDialogViewModel context))
                return string.Empty;

            return context.Text;
        }
    }
}
