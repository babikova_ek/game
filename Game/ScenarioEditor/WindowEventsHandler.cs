﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using ScenarioEditor.Dialogs;
using ScenarioEditor.Model;
using ScenarioEditor.Properties;

namespace ScenarioEditor
{
    public class WindowEventsHandler
    {
        private bool _isDrawingEdge;
        private Line _currentEdge;
        private IShapeItem _initialVertex;

        private readonly ElementManager _elementManager;
        private readonly ConnectionPainter _connectionPainter;
        private readonly CustomDialogs _dialogs;
        private readonly Canvas _mainCanvas;

        public WindowEventsHandler(CustomDialogs dialogs, ElementManager elementManager)
        {
            _dialogs = dialogs;
            _elementManager = elementManager;

            _mainCanvas = MainWindowResources.GetCanvas(Resources.MainCanvas);
            _connectionPainter = new ConnectionPainter(_elementManager, _mainCanvas);
        }
        
        public void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            var hasVertexUnderMouse = Common.TryGetAnyVertexUnderMouse(
                MousePosition.CurrentMousePosition,
                _elementManager, 
                out var vertexUnderMouse);

            _initialVertex = vertexUnderMouse;

            if (hasVertexUnderMouse)
            {
                ProcessClickOnVertex(vertexUnderMouse, e);
            }
            else
            {
                ProcessClickOnWhiteSpace();
            }
        }

        private void ProcessClickOnWhiteSpace()
        {
            _elementManager.AddNewEllipseVertex();
        }

        private void ProcessClickOnVertex(IShapeItem vertexUnderMouse, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                ProcessDoubleClickOnVertex(vertexUnderMouse);
            }
            else
            {
                ProcessSingleClickOnVertex(vertexUnderMouse);
            }
        }

        private void ProcessDoubleClickOnVertex(IShapeItem vertexUnderMouse)
        {
            Common.EditTextInVertex(vertexUnderMouse, _dialogs);
        }

        public void OnMouseMove(MouseEventArgs e)
        {
            MousePosition.CurrentMousePosition = e.GetPosition(_mainCanvas);

            if (_isDrawingEdge)
            {
                ContinueDrawingEdge();
            }
        }

        private void ContinueDrawingEdge()
        {
            var hasVertexUnderMouse = Common.TryGetVertexUnderMouse(MousePosition.CurrentMousePosition, _elementManager, out var vertexUnderMouse);

            if (hasVertexUnderMouse)
            {
                MoveEdgeToVertexCenter(vertexUnderMouse);
            }
            else
            {
                MoveEdgeToNewMusePosition();
            }
        }

        private void MoveEdgeToNewMusePosition()
        {
            _currentEdge.X2 = MousePosition.CurrentMousePosition.X;
            _currentEdge.Y2 = MousePosition.CurrentMousePosition.Y;
        }

        private void MoveEdgeToVertexCenter(IShapeItem vertexUnderMouse)
        {
            var vertexCenter = Common.GetVertexCenter(vertexUnderMouse.Vertex);

            _currentEdge.X2 = vertexCenter.X;
            _currentEdge.Y2 = vertexCenter.Y;
        }

        public void OnMouseLeftButtonUp()
        {
            if (_isDrawingEdge)
            {
                StopDrawingEdge();
            }
        }

        private void StopDrawingEdge()
        {
            _isDrawingEdge = false;

            var hasVertexUnderMouse = Common.TryGetVertexUnderMouse(MousePosition.CurrentMousePosition, _elementManager, out var vertexUnderMouse);

            if (hasVertexUnderMouse)
            {
                _connectionPainter.AddNewConnection(vertexUnderMouse, _currentEdge, _initialVertex);
            }
            else
            {
                _mainCanvas.Children.RemoveAt(0);
                _elementManager.Edges.RemoveAt(_elementManager.Edges.Count - 1);
            }
        }

        public void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            MousePosition.RightButtonClickPosition = e.GetPosition(_mainCanvas);
        }

        private void ProcessSingleClickOnVertex(IShapeItem vertexUnderMouse)
        {
            if (!(vertexUnderMouse is RectItem))
            {
                _currentEdge = _connectionPainter.StartConnectingVertexes(vertexUnderMouse.Vertex);

                var edge = new EdgeItem { Edge = _currentEdge };

                _elementManager.CurrentEdge = edge;
                _elementManager.Edges.Add(edge);
                _mainCanvas.Children.Insert(0, _currentEdge);

                _isDrawingEdge = true;
            }
        }
    }
}
