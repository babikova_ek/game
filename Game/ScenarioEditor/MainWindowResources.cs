﻿using System.Windows;
using System.Windows.Controls;

namespace ScenarioEditor
{
    public class MainWindowResources
    {
        private static Window _window;

        public MainWindowResources(Window window)
        {
            _window = window;
        }

        public static Style GetStyle(string name)
        {
            return _window.Resources[name] as Style;
        }

        public static Canvas GetCanvas(string name)
        {
            return _window.FindName(name) as Canvas;
        }

        public static ContextMenu GetContextMenu(string name)
        {
            return _window.Resources[name] as ContextMenu;
        }
    }
}
