﻿using System.Windows;

namespace ScenarioEditor
{
    public static class MousePosition
    {
        public static Point CurrentMousePosition { get; set; }
        public static Point RightButtonClickPosition { get; set; }
    }
}
