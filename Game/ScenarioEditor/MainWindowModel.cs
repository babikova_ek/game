﻿using System;
using ScenarioEditor.Commands;
using ScenarioEditor.Dialogs;

namespace ScenarioEditor
{
    internal class MainWindowModel
    {
        public MainWindowModel(ElementManager elementManager, CustomDialogs dialogs, Action close)
        {
            ContextMenuCommands = new ContextMenuCommands(elementManager, dialogs);
            MenuCommands = new MenuCommands(elementManager, dialogs, close);
        }

        public MenuCommands MenuCommands { get; }
        public ContextMenuCommands ContextMenuCommands { get; }
    }
}
